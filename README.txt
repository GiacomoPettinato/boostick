​Endless hypercasual game for Android. 
Climb a giant candy stick by throwing a marshmellow 
and avoid sweet obstacles!


OVERVIEW

Bad Seed as client

1 month of development

16 members (5 designers, 4 programmers, 3 concept artists, 4 3D artists)


WHAT I DID

Endless level building by procedural generation system

Character controller & FSM animations

Obstacle behaviours

Tools for testing and tower composition

VFX & Post-processing